package com.kushagra.socialrep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kushagra.socialrep.entity.Topic;
import com.kushagra.socialrep.service.TopicService;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService topicService;

	@PostMapping("/topics")
	public Topic saveTopic(@RequestBody Topic topic) {
		return topicService.saveTopic(topic);
		
	}
}
