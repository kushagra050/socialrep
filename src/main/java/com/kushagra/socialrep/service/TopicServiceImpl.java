package com.kushagra.socialrep.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kushagra.socialrep.entity.Topic;
import com.kushagra.socialrep.repository.TopicRepository;

@Service
public class TopicServiceImpl implements TopicService {

	@Autowired
	TopicRepository topicRepository;
	public Topic saveTopic(Topic topic) {
		return topicRepository.save(topic);
	}

}
