package com.kushagra.socialrep.service;

import org.springframework.stereotype.Service;

import com.kushagra.socialrep.entity.Topic;


public interface TopicService {

	public Topic saveTopic(Topic topic);
}
