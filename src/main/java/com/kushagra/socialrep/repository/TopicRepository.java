package com.kushagra.socialrep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kushagra.socialrep.entity.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic,Long> {

}
