package com.kushagra.socialrep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kushagra.socialrep.entity.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region,Long> {

}
