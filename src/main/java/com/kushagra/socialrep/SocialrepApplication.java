package com.kushagra.socialrep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialrepApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialrepApplication.class, args);
	}

}
